package com.utc.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.utc.domain.enumeration.TypeDoc;

/**
 * A DocumentEntity.
 */
@Entity
@Table(name = "document_entity")
@Document(indexName = "documententity")
public class DocumentEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private TypeDoc type;

    @Column(name = "review")
    private String review;

    @NotNull
    @Column(name = "author", nullable = false)
    private String author;

    @NotNull
    @Column(name = "publisher", nullable = false)
    private String publisher;

    @NotNull
    @Column(name = "year", nullable = false)
    private Integer year;

    @Column(name = "topics")
    private String topics;

    @NotNull
    @Column(name = "price", nullable = false)
    private Integer price;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "content_content_type")
    private String contentContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public DocumentEntity title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TypeDoc getType() {
        return type;
    }

    public DocumentEntity type(TypeDoc type) {
        this.type = type;
        return this;
    }

    public void setType(TypeDoc type) {
        this.type = type;
    }

    public String getReview() {
        return review;
    }

    public DocumentEntity review(String review) {
        this.review = review;
        return this;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getAuthor() {
        return author;
    }

    public DocumentEntity author(String author) {
        this.author = author;
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public DocumentEntity publisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Integer getYear() {
        return year;
    }

    public DocumentEntity year(Integer year) {
        this.year = year;
        return this;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getTopics() {
        return topics;
    }

    public DocumentEntity topics(String topics) {
        this.topics = topics;
        return this;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public Integer getPrice() {
        return price;
    }

    public DocumentEntity price(Integer price) {
        this.price = price;
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public byte[] getContent() {
        return content;
    }

    public DocumentEntity content(byte[] content) {
        this.content = content;
        return this;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public DocumentEntity contentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
        return this;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DocumentEntity documentEntity = (DocumentEntity) o;
        if (documentEntity.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, documentEntity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DocumentEntity{" +
            "id=" + id +
            ", title='" + title + "'" +
            ", type='" + type + "'" +
            ", review='" + review + "'" +
            ", author='" + author + "'" +
            ", publisher='" + publisher + "'" +
            ", year='" + year + "'" +
            ", topics='" + topics + "'" +
            ", price='" + price + "'" +
            ", content='" + content + "'" +
            ", contentContentType='" + contentContentType + "'" +
            '}';
    }
}
