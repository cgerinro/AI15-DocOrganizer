package com.utc.domain.enumeration;

/**
 * The TypeDoc enumeration.
 */
public enum TypeDoc {
    ARTICLE,ESSAY,BOOK,JUDGEDECISION
}
