package com.utc.domain;

import java.io.Serializable;

/**
 * Created by oscar on 18/01/17.
 */
public class DocumentScore implements Serializable {
    private DocumentEntity document;

    private Long score;

    public DocumentScore(DocumentEntity document, Long score) {
        this.document = document;
        this.score = score;
    }

    public DocumentEntity getDocument() {

        return document;
    }

    public DocumentScore document(DocumentEntity document) {
        this.document = document;
        return this;
    }

    public void setDocument(DocumentEntity document) {
        this.document = document;
    }

    public Long getScore() {
        return score;
    }

    public DocumentScore score(Long score) {
        this.score = score;
        return this;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocumentScore that = (DocumentScore) o;

        if (!document.equals(that.document)) return false;
        return score.equals(that.score);
    }

    @Override
    public int hashCode() {
        int result = document.hashCode();
        result = 31 * result + score.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DocumentScore{" +
            "document=" + document +
            ", score=" + score +
            '}';
    }
}
