package com.utc.service;

import com.utc.domain.Keyword;
import com.utc.repository.KeywordRepository;
import com.utc.repository.search.KeywordSearchRepository;
import com.utc.service.DocumentEntityService;
import com.utc.domain.DocumentEntity;
import com.utc.repository.DocumentEntityRepository;
import com.utc.repository.search.DocumentEntitySearchRepository;
import org.elasticsearch.search.aggregations.InternalAggregation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

import org.elasticsearch.search.aggregations.AggregationBuilders;

/**
 * Service Implementation for managing DocumentEntity.
 */
@Service
@Transactional
public class KeywordService {

    private final Logger log = LoggerFactory.getLogger(KeywordService.class);

    @Inject
    private KeywordRepository keywordRepository;

    @Inject
    private KeywordSearchRepository keywordSearchRepository;

    /**
     * Save a keyword.
     *
     * @param keyword the entity to save
     * @return the persisted entity
     */
    public Keyword save(Keyword keyword) {
        log.debug("Request to save Keyword : {}", keyword);
        System.out.println(keyword);
        Keyword result = keywordRepository.save(keyword);
        keywordSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the keywords.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Keyword> findAll(Pageable pageable) {
        log.debug("Request to get all Keywords");
        Page<Keyword> result = keywordRepository.findAll(pageable);
        return result;
    }

    /**
     * Search for the keyword corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Keyword> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DocumentEntities for query {}", query);
        Page<Keyword> result = keywordSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
