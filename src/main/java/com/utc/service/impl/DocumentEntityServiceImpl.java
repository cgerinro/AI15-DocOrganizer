package com.utc.service.impl;

import com.utc.service.DocumentEntityService;
import com.utc.domain.DocumentEntity;
import com.utc.repository.DocumentEntityRepository;
import com.utc.repository.search.DocumentEntitySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DocumentEntity.
 */
@Service
@Transactional
public class DocumentEntityServiceImpl implements DocumentEntityService{

    private final Logger log = LoggerFactory.getLogger(DocumentEntityServiceImpl.class);

    @Inject
    private DocumentEntityRepository documentEntityRepository;

    @Inject
    private DocumentEntitySearchRepository documentEntitySearchRepository;

    /**
     * Save a documentEntity.
     *
     * @param documentEntity the entity to save
     * @return the persisted entity
     */
    public DocumentEntity save(DocumentEntity documentEntity) {
        log.debug("Request to save DocumentEntity : {}", documentEntity);
        DocumentEntity result = documentEntityRepository.save(documentEntity);
        documentEntitySearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the documentEntities.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentEntity> findAll(Pageable pageable) {
        log.debug("Request to get all DocumentEntities");
        Page<DocumentEntity> result = documentEntityRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one documentEntity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DocumentEntity findOne(Long id) {
        log.debug("Request to get DocumentEntity : {}", id);
        DocumentEntity documentEntity = documentEntityRepository.findOne(id);
        return documentEntity;
    }

    /**
     *  Delete the  documentEntity by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DocumentEntity : {}", id);
        documentEntityRepository.delete(id);
        documentEntitySearchRepository.delete(id);
    }

    /**
     * Search for the documentEntity corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentEntity> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DocumentEntities for query {}", query);
        Page<DocumentEntity> result = documentEntitySearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
