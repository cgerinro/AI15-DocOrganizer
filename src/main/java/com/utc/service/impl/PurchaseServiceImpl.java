package com.utc.service.impl;

import com.utc.domain.DocumentEntity;
import com.utc.domain.DocumentScore;
import com.utc.service.PurchaseService;
import com.utc.domain.Purchase;
import com.utc.domain.User;
import com.utc.repository.PurchaseRepository;
import com.utc.repository.search.PurchaseSearchRepository;
import io.undertow.security.api.SecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Purchase.
 */
@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService{

    private final Logger log = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    @Inject
    private PurchaseRepository purchaseRepository;

    @Inject
    private PurchaseSearchRepository purchaseSearchRepository;

    /**
     * Save a purchase.
     *
     * @param purchase the entity to save
     * @return the persisted entity
     */
    public Purchase save(Purchase purchase) {
        log.debug("Request to save Purchase : {}", purchase);
        Purchase result = purchaseRepository.save(purchase);
        purchaseSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the purchases.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Purchase> findAll(Pageable pageable) {
        log.debug("Request to get all Purchases");
        Page<Purchase> result = purchaseRepository.findAll(pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Purchase> findAllByClient(User c, Pageable pageable){
        log.debug("Request to get all Purchases of a client");
        return purchaseRepository.findAllByClient(c, pageable);
    }

    /**
     *  Get one purchase by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Purchase findOne(Long id) {
        log.debug("Request to get Purchase : {}", id);
        Purchase purchase = purchaseRepository.findOne(id);
        return purchase;
    }

    /**
     *  Delete the  purchase by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Purchase : {}", id);
        purchaseRepository.delete(id);
        purchaseSearchRepository.delete(id);
    }

    /**
     * Search for the purchase corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Purchase> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Purchases for query {}", query);
        Page<Purchase> result = purchaseSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    /**
     * Get the rank of the purchase
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DocumentScore> rank(Pageable pageable) {
        log.debug("Request to get the rank of the purchases");
        Map<DocumentEntity, Long> map = purchaseRepository.findAll(pageable)
            .getContent().stream().collect(
                Collectors.groupingBy(
                    Purchase::getDocument, Collectors.counting()
                )
            );

        Map<DocumentEntity, Long> sortedMap = new LinkedHashMap<>();

        //Sort a map and add to finalMap
        map.entrySet().stream()
            .sorted(Map.Entry.<DocumentEntity, Long>comparingByValue()
                .reversed()).forEachOrdered(e -> sortedMap.put(e.getKey(), e.getValue()));

        List<DocumentScore> result = new ArrayList<DocumentScore>();

        map.forEach((key, value) -> result.add(new DocumentScore(key, value)));

        return new PageImpl<DocumentScore>(result, pageable, 20);
    }

    /**
     * Get top n purchases by a parameter
     *
     *  @param n the number of the top
     *  @param type The type of the top
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Map<String, Long> top(Pageable pageable, int n, String type) {
        log.debug("Request to get the top n of the purchases");
        Map<String, Long> map = purchaseRepository.findAll(pageable).map(x -> {
            if (Objects.equals(type, "author")) return x.getDocument().getAuthor();
            if (Objects.equals(type, "topics")) return x.getDocument().getTopics();
            if (Objects.equals(type, "publisher")) return x.getDocument().getPublisher();
            else return x.getDocument().getTitle();
        }).getContent().stream()
            .collect(
                Collectors.groupingBy(
                    Function.identity(), Collectors.counting()
                )
            );

        Map<String, Long> sortedMap = new LinkedHashMap<>();

        //Sort a map and add to finalMap
        map.entrySet().stream()
            .sorted(Map.Entry.<String, Long>comparingByValue()
                .reversed()).forEachOrdered(e -> sortedMap.put(e.getKey(), e.getValue()));

        return sortedMap;
    }
}
