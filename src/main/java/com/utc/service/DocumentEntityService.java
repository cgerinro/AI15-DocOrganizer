package com.utc.service;

import com.utc.domain.DocumentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing DocumentEntity.
 */
public interface DocumentEntityService {

    /**
     * Save a documentEntity.
     *
     * @param documentEntity the entity to save
     * @return the persisted entity
     */
    DocumentEntity save(DocumentEntity documentEntity);

    /**
     *  Get all the documentEntities.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DocumentEntity> findAll(Pageable pageable);

    /**
     *  Get the "id" documentEntity.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DocumentEntity findOne(Long id);

    /**
     *  Delete the "id" documentEntity.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the documentEntity corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DocumentEntity> search(String query, Pageable pageable);
}
