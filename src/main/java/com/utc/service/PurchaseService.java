package com.utc.service;

import com.utc.domain.DocumentScore;
import com.utc.domain.Purchase;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;

import com.utc.domain.User;
/**
 * Service Interface for managing Purchase.
 */
public interface PurchaseService {

    /**
     * Save a purchase.
     *
     * @param purchase the entity to save
     * @return the persisted entity
     */
    Purchase save(Purchase purchase);

    /**
     *  Get all the purchases.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Purchase> findAll(Pageable pageable);

    /**
     * Get all purchases of one client
     *
     * @param user the client
     * @return the list of entities
     */
    Page<Purchase> findAllByClient(User user, Pageable pageable);


    /**
     *  Get the "id" purchase.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Purchase findOne(Long id);

    /**
     *  Delete the "id" purchase.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the purchase corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Purchase> search(String query, Pageable pageable);

    /**
     * Get the rank of the purchases
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DocumentScore> rank(Pageable pageable);

    /**
     * Get the top of the purchases by a parameter
     *
     *  @param n number of the top
     *  @param type type of the top
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Map<String, Long> top(Pageable pageable, int n, String type);
}
