package com.utc.repository;

import com.utc.domain.DocumentEntity;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DocumentEntity entity.
 */
@SuppressWarnings("unused")
public interface DocumentEntityRepository extends JpaRepository<DocumentEntity,Long> {

}
