package com.utc.repository;

import com.utc.domain.Keyword;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Keyword entity.
 */
@SuppressWarnings("unused")
public interface KeywordRepository extends JpaRepository<Keyword,Long> {

}
