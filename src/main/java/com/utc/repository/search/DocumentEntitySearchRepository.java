package com.utc.repository.search;

import com.utc.domain.DocumentEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the DocumentEntity entity.
 */
public interface DocumentEntitySearchRepository extends ElasticsearchRepository<DocumentEntity, Long> {
}
