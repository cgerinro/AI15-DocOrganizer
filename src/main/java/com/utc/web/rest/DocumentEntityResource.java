package com.utc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.utc.domain.DocumentEntity;
import com.utc.service.DocumentEntityService;
import com.utc.web.rest.util.HeaderUtil;
import com.utc.web.rest.util.PaginationUtil;

import io.swagger.annotations.ApiParam;
import io.undertow.security.api.SecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DocumentEntity.
 */
@RestController
@RequestMapping("/api")
public class DocumentEntityResource {

    private final Logger log = LoggerFactory.getLogger(DocumentEntityResource.class);

    @Inject
    private DocumentEntityService documentEntityService;

    /**
     * POST  /document-entities : Create a new documentEntity.
     *
     * @param documentEntity the documentEntity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentEntity, or with status 400 (Bad Request) if the documentEntity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/document-entities")
    @Timed
    public ResponseEntity<DocumentEntity> createDocumentEntity(@Valid @RequestBody DocumentEntity documentEntity) throws URISyntaxException {
        log.debug("REST request to save DocumentEntity : {}", documentEntity);
        if (documentEntity.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("documentEntity", "idexists", "A new documentEntity cannot already have an ID")).body(null);
        }
        User u = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        documentEntity.setPublisher(u.getUsername());
        DocumentEntity result = documentEntityService.save(documentEntity);
        return ResponseEntity.created(new URI("/api/document-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("documentEntity", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /document-entities : Updates an existing documentEntity.
     *
     * @param documentEntity the documentEntity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documentEntity,
     * or with status 400 (Bad Request) if the documentEntity is not valid,
     * or with status 500 (Internal Server Error) if the documentEntity couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/document-entities")
    @Timed
    public ResponseEntity<DocumentEntity> updateDocumentEntity(@Valid @RequestBody DocumentEntity documentEntity) throws URISyntaxException {
        log.debug("REST request to update DocumentEntity : {}", documentEntity);
        if (documentEntity.getId() == null) {
            return createDocumentEntity(documentEntity);
        }
        DocumentEntity result = documentEntityService.save(documentEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("documentEntity", documentEntity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /document-entities : get all the documentEntities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of documentEntities in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/document-entities")
    @Timed
    public ResponseEntity<List<DocumentEntity>> getAllDocumentEntities(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DocumentEntities");
        Page<DocumentEntity> page = documentEntityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/document-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /document-entities/:id : get the "id" documentEntity.
     *
     * @param id the id of the documentEntity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documentEntity, or with status 404 (Not Found)
     */
    @GetMapping("/document-entities/{id}")
    @Timed
    public ResponseEntity<DocumentEntity> getDocumentEntity(@PathVariable Long id) {
        log.debug("REST request to get DocumentEntity : {}", id);
        DocumentEntity documentEntity = documentEntityService.findOne(id);
        return Optional.ofNullable(documentEntity)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /document-entities/:id : delete the "id" documentEntity.
     *
     * @param id the id of the documentEntity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/document-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocumentEntity(@PathVariable Long id) {
        log.debug("REST request to delete DocumentEntity : {}", id);
        documentEntityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("documentEntity", id.toString())).build();
    }

    /**
     * SEARCH  /_search/document-entities?query=:query : search for the documentEntity corresponding
     * to the query.
     *
     * @param query the query of the documentEntity search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/document-entities")
    @Timed
    public ResponseEntity<List<DocumentEntity>> searchDocumentEntities(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of DocumentEntities for query {}", query);
        Page<DocumentEntity> page = documentEntityService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/document-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
