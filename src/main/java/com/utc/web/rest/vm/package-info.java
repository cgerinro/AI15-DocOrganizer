/**
 * View Models used by Spring MVC REST controllers.
 */
package com.utc.web.rest.vm;
