package com.utc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.utc.domain.Keyword;

import com.utc.repository.KeywordRepository;
import com.utc.repository.search.KeywordSearchRepository;
import com.utc.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Keyword.
 */
@RestController
@RequestMapping("/api")
public class KeywordResource {

    private final Logger log = LoggerFactory.getLogger(KeywordResource.class);

    @Inject
    private KeywordRepository keywordRepository;

    @Inject
    private KeywordSearchRepository keywordSearchRepository;

    /**
     * POST  /keywords : Create a new keyword.
     *
     * @param keyword the keyword to create
     * @return the ResponseEntity with status 201 (Created) and with body the new keyword, or with status 400 (Bad Request) if the keyword has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/keywords")
    @Timed
    public ResponseEntity<Keyword> createKeyword(@Valid @RequestBody Keyword keyword) throws URISyntaxException {
        log.debug("REST request to save Keyword : {}", keyword);
        if (keyword.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("keyword", "idexists", "A new keyword cannot already have an ID")).body(null);
        }
        Keyword result = keywordRepository.save(keyword);
        keywordSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/keywords/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("keyword", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /keywords : Updates an existing keyword.
     *
     * @param keyword the keyword to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated keyword,
     * or with status 400 (Bad Request) if the keyword is not valid,
     * or with status 500 (Internal Server Error) if the keyword couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/keywords")
    @Timed
    public ResponseEntity<Keyword> updateKeyword(@Valid @RequestBody Keyword keyword) throws URISyntaxException {
        log.debug("REST request to update Keyword : {}", keyword);
        if (keyword.getId() == null) {
            return createKeyword(keyword);
        }
        Keyword result = keywordRepository.save(keyword);
        keywordSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("keyword", keyword.getId().toString()))
            .body(result);
    }

    /**
     * GET  /keywords : get all the keywords.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of keywords in body
     */
    @GetMapping("/keywords")
    @Timed
    public List<Keyword> getAllKeywords() {
        log.debug("REST request to get all Keywords");
        List<Keyword> keywords = keywordRepository.findAll();
        return keywords;
    }

    /**
     * GET  /keywords/rank : get keywords rank.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of keywords in body
     */
    @GetMapping("/keywords/rank")
    @Timed
    public Map<String, Long> getRankKeyword() {
        log.debug("REST request to get rank Keywords");
        List<Keyword> keywords = keywordRepository.findAll();
        Map<String, Long> result = keywords.stream().map(Keyword::getValue).collect(
            Collectors.groupingBy(
                Function.identity(), Collectors.counting()
            )
        );

        Map<String, Long> finalMap = new LinkedHashMap<>();

        result.entrySet().stream()
            .sorted(Map.Entry.<String, Long>comparingByValue()
                .reversed())
            .limit(15).forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));

        return finalMap;
    }

    /**
     * GET  /keywords/rank/:type : get keywords rank by type.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of keywords in body
     */
    @GetMapping("/keywords/rank/{type}")
    @Timed
    public Map<String, Long> getRankKeywordByType(@PathVariable String type) {
        log.debug("REST request to get rank Keywords by type");
        List<Keyword> keywords = keywordRepository.findAll();
        Map<String, Long> result = keywords.stream()
            .filter(f -> Objects.equals(f.getType(), type))
            .map(Keyword::getValue).collect(
            Collectors.groupingBy(
                Function.identity(), Collectors.counting()
            )
        );

        Map<String, Long> finalMap = new LinkedHashMap<>();

        result.entrySet().stream()
            .sorted(Map.Entry.<String, Long>comparingByValue()
                .reversed())
            .limit(15).forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));

        return finalMap;
    }

    /**
     * GET  /keywords/:id : get the "id" keyword.
     *
     * @param id the id of the keyword to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the keyword, or with status 404 (Not Found)
     */
    @GetMapping("/keywords/{id}")
    @Timed
    public ResponseEntity<Keyword> getKeyword(@PathVariable Long id) {
        log.debug("REST request to get Keyword : {}", id);
        Keyword keyword = keywordRepository.findOne(id);
        return Optional.ofNullable(keyword)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /keywords/:id : delete the "id" keyword.
     *
     * @param id the id of the keyword to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/keywords/{id}")
    @Timed
    public ResponseEntity<Void> deleteKeyword(@PathVariable Long id) {
        log.debug("REST request to delete Keyword : {}", id);
        keywordRepository.delete(id);
        keywordSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("keyword", id.toString())).build();
    }

    /**
     * SEARCH  /_search/keywords?query=:query : search for the keyword corresponding
     * to the query.
     *
     * @param query the query of the keyword search
     * @return the result of the search
     */
    @GetMapping("/_search/keywords")
    @Timed
    public List<Keyword> searchKeywords(@RequestParam String query) {
        log.debug("REST request to search Keywords for query {}", query);
        return StreamSupport
            .stream(keywordSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }


}
