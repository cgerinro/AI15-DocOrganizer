
  ${AnsiColor.BLUE}███████ ${AnsiColor.BLUE} ████████  ███████  ██        ████████  ████████
  ${AnsiColor.BLUE}██     █${AnsiColor.BLUE}    ██     ██    ██ ██           ██     ██    ██
  ${AnsiColor.WHITE}███████ ${AnsiColor.WHITE}    ██     ███████  ██           ██     ██    ██
  ${AnsiColor.WHITE}██     █${AnsiColor.WHITE}    ██     ██    ██ ██           ██     ██    ██
  ${AnsiColor.RED}███████ ${AnsiColor.RED} ████████  ███████  ████████  ████████  ████████

${AnsiColor.BRIGHT_BLUE}  :: Running Spring Boot ${spring-boot.version} ::
:: http://jhipster.github.io ::${AnsiColor.DEFAULT}
