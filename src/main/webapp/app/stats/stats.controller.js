(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('StatsController', StatsController);

    StatsController.$inject = ['Keyword', 'TopPurchasesByType'];

    function StatsController (Keyword, TopPurchasesByType) {
        var vm = this;

        vm.keywordsGlobal= {};
        vm.keywordsTitle = {};
        vm.keywordsAuthor = {};
        vm.keywordsPublisher = {};
        vm.keywordsTopic = {};

        vm.topAuthors = {};
        vm.topPublishers = {};

        Keyword.query({}, onSuccessGlobal, onError);
        Keyword.query({type: "title"}, onSuccessTitle, onError);
        Keyword.query({type: "author"}, onSuccessAuthor, onError);
        Keyword.query({type: "publisher"}, onSuccessPublisher, onError);
        Keyword.query({type: "topic"}, onSuccessTopic, onError);

        TopPurchasesByType.query({type: "author"}, onSuccessTopAuthors, onError);
        TopPurchasesByType.query({type: "author"}, onSuccessTopPublishers, onError);

        function onSuccessGlobal(data, headers) {
            vm.keywordsGlobal = data;
        }

        function onSuccessTitle(data, headers) {
            vm.keywordsTitle = data;
        }

        function onSuccessAuthor(data, headers) {
            vm.keywordsAuthor = data;
        }

        function onSuccessPublisher(data, headers) {
            vm.keywordsPublisher = data;
        }

        function onSuccessTopic(data, headers) {
            vm.keywordsTopic = data;
        }

        function onSuccessTopAuthors(data, headers) {
            vm.topAuthors = data;
        }

        function onSuccessTopPublishers(data, headers) {
            vm.topPublishers = data;
        }

        function onError(e) {
            console.error(e)
        }
    }
})();
