(function() {
    'use strict';
    angular
        .module('libraryApp')
        .factory('Keyword', Keyword);

    Keyword.$inject = ['$resource'];

    function Keyword ($resource) {
        var resourceUrl =  'api/keywords/rank/:type';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET'},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
        });
    }
})();
