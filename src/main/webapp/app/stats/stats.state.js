(function() {
    'use strict';

    angular
        .module('libraryApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('stats', {
            parent: 'app',
            url: '/stats',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'Statistics'
            },
            views: {
                'content@': {
                    templateUrl: 'app/stats/stats.html',
                    controller: 'StatsController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
