(function() {
    'use strict';
    angular
        .module('libraryApp')
        .factory('TopPurchasesByType', TopPurchasesByType);

    TopPurchasesByType.$inject = ['$resource'];

    function TopPurchasesByType ($resource) {
        var resourceUrl =  'api/purchases/top/:type';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET'},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
        });
    }
})();
