/**
 * Created by gcamille on 18/01/17.
 */
(function() {
    'use strict';

    angular
        .module('libraryApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('topPurchase', {
            parent: 'app',
            url: '/topPurchase',
            data: {
                authorities: ['ROLE_USER','ROLE_PUBLISHER'],
                pageTitle: 'Most purchased documents'
            },
            views: {
                'content@': {
                    templateUrl: 'app/top-purchase/top-purchase.html',
                    controller: 'TopPurchaseController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
