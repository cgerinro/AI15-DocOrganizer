/**
 * Created by gcamille on 18/01/17.
 */
(function() {
    'use strict';
    angular
        .module('libraryApp')
        .factory('TopPurchase', TopPurchase);

    TopPurchase.$inject = ['$resource'];

    function TopPurchase ($resource) {
        var resourceUrl =  '/api/purchases/rank';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
        });
    }
})();
