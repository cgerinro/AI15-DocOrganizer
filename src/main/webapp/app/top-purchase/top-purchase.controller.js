(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('TopPurchaseController', TopPurchaseController);

    TopPurchaseController.$inject = ['TopPurchase'];

    function TopPurchaseController (TopPurchase) {
        var vm = this;
        vm.topPurchase = [];
        //vm.openFile = DocumentEntity.openFile;
        TopPurchase.query({},onSuccess,onError)

        function onError(e) {
            console.log("AN ERROR OCCURED")
            console.error(e)
        }

        function onSuccess(data){
            console.log(data)
            vm.topPurchase=data;
        }

    }
})();
