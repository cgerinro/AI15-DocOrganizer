(function() {
    'use strict';
    angular
        .module('libraryApp')
        .factory('Purchase', Purchase);

    Purchase.$inject = ['$resource', 'DateUtils'];

    function Purchase ($resource, DateUtils) {
        var resourceUrl =  'api/purchases/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.purchaseDate = DateUtils.convertDateTimeFromServer(data.purchaseDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
