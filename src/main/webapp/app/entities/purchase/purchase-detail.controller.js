(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('PurchaseDetailController', PurchaseDetailController);

    PurchaseDetailController.$inject = ['$scope', 'DataUtils', '$rootScope', '$stateParams', 'previousState', 'entity', 'Purchase', 'DocumentEntity', 'User'];

    function PurchaseDetailController($scope, DataUtils, $rootScope, $stateParams, previousState, entity, Purchase, DocumentEntity, User) {
        var vm = this;

        vm.purchase = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('libraryApp:purchaseUpdate', function(event, result) {
            vm.purchase = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
