(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('PurchaseDialogController', PurchaseDialogController);

    PurchaseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Purchase', 'DocumentEntity', 'User'];

    function PurchaseDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Purchase, DocumentEntity, User) {
        var vm = this;

        vm.purchase = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.documents = DocumentEntity.query({filter: 'purchase-is-null'});
        $q.all([vm.purchase.$promise, vm.documents.$promise]).then(function() {
            if (!vm.purchase.document || !vm.purchase.document.id) {
                return $q.reject();
            }
            return DocumentEntity.get({id : vm.purchase.document.id}).$promise;
        }).then(function(document) {
            vm.documents.push(document);
        });
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.purchase.id !== null) {
                Purchase.update(vm.purchase, onSaveSuccess, onSaveError);
            } else {
                Purchase.save(vm.purchase, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('libraryApp:purchaseUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.purchaseDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
