(function() {
    'use strict';
    angular
        .module('libraryApp')
        .factory('DocumentEntity', DocumentEntity);

    DocumentEntity.$inject = ['$resource'];

    function DocumentEntity ($resource) {
        var resourceUrl =  'api/document-entities/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
