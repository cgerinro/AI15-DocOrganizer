(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('DocumentEntityDialogController', DocumentEntityDialogController);

    DocumentEntityDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'DocumentEntity'];

    function DocumentEntityDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, DocumentEntity) {
        var vm = this;

        vm.documentEntity = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.documentEntity.id !== null) {
                DocumentEntity.update(vm.documentEntity, onSaveSuccess, onSaveError);
            } else {
                DocumentEntity.save(vm.documentEntity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('libraryApp:documentEntityUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setContent = function ($file, documentEntity) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        documentEntity.content = base64Data;
                        documentEntity.contentContentType = $file.type;
                    });
                });
            }
        };

    }
})();
