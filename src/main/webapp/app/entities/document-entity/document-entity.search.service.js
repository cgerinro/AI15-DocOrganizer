(function() {
    'use strict';

    angular
        .module('libraryApp')
        .factory('DocumentEntitySearch', DocumentEntitySearch);

    DocumentEntitySearch.$inject = ['$resource'];

    function DocumentEntitySearch($resource) {
        var resourceUrl =  'api/_search/document-entities/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
