(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('DocumentEntityDetailController', DocumentEntityDetailController);

    DocumentEntityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'Principal', 'Purchase', 'previousState', 'DataUtils', 'entity', 'DocumentEntity', 'User'];

    function DocumentEntityDetailController($scope, $rootScope, $stateParams, Principal, Purchase, previousState, DataUtils, entity, DocumentEntity, User) {
        var vm = this;

        vm.documentEntity = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.buy = buy;
        vm.client = null;

        Principal.identity().then(function(account) {
            User.query().$promise.then(function(users) {
                vm.client = users.find(function(user) {
                    return user.login == account.login
                })
            });
        });

        var unsubscribe = $rootScope.$on('libraryApp:documentEntityUpdate', function(event, result) {
            vm.documentEntity = result;
        });

        function buy() {
            vm.isSaving = true;
            var purchase = {
                client: vm.client,
                document: vm.documentEntity,
                purchaseDate: new Date(),
                id: null
            };
            Purchase.save(purchase, onSaveSuccess, onSaveError);
        }

        function onSaveSuccess (result) {
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        $scope.$on('$destroy', unsubscribe);
    }
})();
