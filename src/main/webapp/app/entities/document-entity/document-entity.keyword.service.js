(function() {
    'use strict';
    angular
        .module('libraryApp')
        .factory('DocumentEntityKeyword', DocumentEntityKeyword);

    DocumentEntityKeyword.$inject = ['$resource'];

    function DocumentEntityKeyword ($resource) {
        var resourceUrl =  'api/keywords';

        return $resource(resourceUrl, {}, {
            'post': { method: 'POST'}
        });
    }
})();
