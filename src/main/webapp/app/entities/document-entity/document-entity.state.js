(function() {
    'use strict';

    angular
        .module('libraryApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('document-entity', {
            parent: 'entity',
            url: '/document-entity?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DocumentEntities'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/document-entity/document-entities.html',
                    controller: 'DocumentEntityController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null,
                searchGlobal: null,
                searchTitle: null,
                searchAuthor: null,
                searchPublisher: null,
                searchTopic: null,
                searchYear: null,
                searchReview: null,
                searchType: "ALL",
                advancedSearch: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search,
                        searchGlobal: $stateParams.searchGlobal,
                        searchTitle: $stateParams.searchTitle,
                        searchAuthor: $stateParams.searchAuthor,
                        searchPublisher: $stateParams.searchPublisher,
                        searchTopic: $stateParams.searchTopic,
                        searchYear: $stateParams.searchYear,
                        searchReview: $stateParams.searchReview,
                        searchType: $stateParams.searchType,
                        advancedSearch: $stateParams.advancedSearch
                    };
                }]
            }
        })
        .state('document-entity-detail', {
            parent: 'entity',
            url: '/document-entity/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DocumentEntity'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/document-entity/document-entity-detail.html',
                    controller: 'DocumentEntityDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DocumentEntity', function($stateParams, DocumentEntity) {
                    return DocumentEntity.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'document-entity',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('document-entity-detail.edit', {
            parent: 'document-entity-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-entity/document-entity-dialog.html',
                    controller: 'DocumentEntityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentEntity', function(DocumentEntity) {
                            return DocumentEntity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('document-entity.new', {
            parent: 'document-entity',
            url: '/new',
            data: {
                authorities: ['ROLE_PUBLISHER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-entity/document-entity-dialog.html',
                    controller: 'DocumentEntityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                type: null,
                                review: null,
                                author: null,
                                publisher: null,
                                year: null,
                                topics: null,
                                price: null,
                                content: null,
                                contentContentType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('document-entity', null, { reload: 'document-entity' });
                }, function() {
                    $state.go('document-entity');
                });
            }]
        })
        .state('document-entity.edit', {
            parent: 'document-entity',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-entity/document-entity-dialog.html',
                    controller: 'DocumentEntityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentEntity', function(DocumentEntity) {
                            return DocumentEntity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('document-entity', null, { reload: 'document-entity' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('document-entity.delete', {
            parent: 'document-entity',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/document-entity/document-entity-delete-dialog.html',
                    controller: 'DocumentEntityDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DocumentEntity', function(DocumentEntity) {
                            return DocumentEntity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('document-entity', null, { reload: 'document-entity' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
