(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('DocumentEntityDeleteController',DocumentEntityDeleteController);

    DocumentEntityDeleteController.$inject = ['$uibModalInstance', 'entity', 'DocumentEntity'];

    function DocumentEntityDeleteController($uibModalInstance, entity, DocumentEntity) {
        var vm = this;

        vm.documentEntity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DocumentEntity.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
