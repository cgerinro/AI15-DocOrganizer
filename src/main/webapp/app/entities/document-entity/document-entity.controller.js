(function() {
    'use strict';

    angular
        .module('libraryApp')
        .controller('DocumentEntityController', DocumentEntityController);

    DocumentEntityController.$inject = ['$scope', '$state', 'DataUtils', 'DocumentEntity', 'DocumentEntityKeyword', 'DocumentEntitySearch', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function DocumentEntityController ($scope, $state, DataUtils, DocumentEntity, DocumentEntityKeyword, DocumentEntitySearch, ParseLinks, AlertService, paginationConstants, pagingParams) {
        var vm = this;

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;
        vm.searchQuery = pagingParams.searchGlobal;
        vm.searchTitle = pagingParams.searchTitle;
        vm.searchAuthor = pagingParams.searchAuthor;
        vm.searchPublisher = pagingParams.searchPublisher;
        vm.searchTopic = pagingParams.searchTopic;
        vm.searchType = pagingParams.searchType;
        vm.searchReview = pagingParams.searchReview;
        vm.searchYear = pagingParams.searchYear;
        vm.advanced = advanced;
        vm.advancedsearch = pagingParams.advancedSearch;
        vm.advancedbutton = vm.advancedsearch ? "hide" : "Advanced search";
        vm.currentSearch = pagingParams.search;
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;

        loadAll();

        $scope.types = ["ALL", "ARTICLE", "ESSAY", "BOOK", "JUDGEDECISION"];

        function loadAll () {
            if (pagingParams.search) {
                DocumentEntitySearch.query({
                    query: pagingParams.search,
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                DocumentEntity.query({
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.documentEntities = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch,
                searchGlobal: vm.searchQuery,
                searchTitle: vm.searchTitle,
                searchAuthor: vm.searchAuthor,
                searchPublisher: vm.searchPublisher,
                searchTopic: vm.searchTopic,
                searchYear: vm.searchYear,
                searchReview: vm.searchReview,
                searchType: vm.searchType,
                advancedSearch: vm.advancedsearch
            });
        }

        function search(searchQuery) {
            if (!searchQuery && !vm.advancedsearch){
                return vm.clear();
            }
            vm.links = null;
            vm.page = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery ? searchQuery : "";
            if (vm.advancedsearch) {
                if (vm.searchTitle) vm.currentSearch =
                    (vm.searchQuery ? "(" + vm.currentSearch + ") AND " : "") + "title:(" + vm.searchTitle + ")";
                if (vm.searchAuthor) vm.currentSearch +=
                    (vm.searchQuery ? "(" + vm.currentSearch + ") AND " : "") + "author:(" + vm.searchAuthor + ")";
                if (vm.searchPublisher) vm.currentSearch +=
                    (vm.searchQuery ? "(" + vm.currentSearch + ") AND " : "") + "publisher:(" + vm.searchPublisher + ")";
                if (vm.searchTopic) vm.currentSearch +=
                    (vm.searchQuery ? "(" + vm.currentSearch + ") AND " : "") + "topics:(" + vm.searchTopic + ")";
                if (vm.searchYear) vm.currentSearch +=
                    (vm.searchQuery ? "(" + vm.currentSearch + ") AND " : "") + "year:(" + vm.searchTopic + ")";
                if (vm.searchReview) vm.currentSearch +=
                    (vm.searchQuery ? "(" + vm.currentSearch + ") AND " : "") + "review:(" + vm.searchReview + ")";
                if (vm.searchType && vm.searchType != "ALL") vm.currentSearch +=
                    (vm.searchQuery ? "(" + vm.currentSearch + ") AND " : "") + "type:(" + vm.searchType + ")";
            }

            console.log(vm.currentSearch);

            addKeywords();

            vm.transition();
        }

        function addKeywords() {
            addAllKeywords(vm.searchQuery);
            if (vm.advancedsearch) {
                addAllKeywords(vm.searchTitle, "title");
                addAllKeywords(vm.searchAuthor, "author");
                addAllKeywords(vm.searchPublisher, "publisher");
                addAllKeywords(vm.searchTopic, "topic");
                addAllKeywords(vm.searchReview, "review");
            }
        }

        function addAllKeywords(str, type) {
            if (str) {
                str.split(" ").forEach(function (s) {
                    addKeyword(s, type)
                })
            }
        }

        function addKeyword(value, type) {
            var newValue = value.replace(/\s/, "");
            if (newValue != "" && type == null) {
                DocumentEntityKeyword.post({value: newValue})
            }
            else if (newValue != "") {
                DocumentEntityKeyword.post({type: type, value: newValue})
            }
        }

        function clear() {
            vm.links = null;
            vm.page = 1;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.currentSearch = null;
            vm.searchAuthor = null;
            vm.searchQuery = null;
            vm.searchTitle = null;
            vm.searchPublisher = null;
            vm.searchTopic = null;
            vm.searchYear = null;
            vm.searchReview = null;
            vm.searchType = "ALL";
            vm.advancedsearch = false;
            vm.transition();
        }

        function advanced() {
            vm.advancedsearch = !vm.advancedsearch;
            if (vm.advancedsearch)
                vm.advancedbutton = "hide";
            else
                vm.advancedbutton = "Advanced search"
        }
    }
})();
