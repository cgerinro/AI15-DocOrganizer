package com.utc.web.rest;

import com.utc.LibraryApp;

import com.utc.domain.DocumentEntity;
import com.utc.repository.DocumentEntityRepository;
import com.utc.service.DocumentEntityService;
import com.utc.repository.search.DocumentEntitySearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.utc.domain.enumeration.TypeDoc;
/**
 * Test class for the DocumentEntityResource REST controller.
 *
 * @see DocumentEntityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LibraryApp.class)
public class DocumentEntityResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final TypeDoc DEFAULT_TYPE = TypeDoc.ARTICLE;
    private static final TypeDoc UPDATED_TYPE = TypeDoc.ESSAY;

    private static final String DEFAULT_REVIEW = "AAAAAAAAAA";
    private static final String UPDATED_REVIEW = "BBBBBBBBBB";

    private static final String DEFAULT_AUTHOR = "AAAAAAAAAA";
    private static final String UPDATED_AUTHOR = "BBBBBBBBBB";

    private static final String DEFAULT_PUBLISHER = "AAAAAAAAAA";
    private static final String UPDATED_PUBLISHER = "BBBBBBBBBB";

    private static final Integer DEFAULT_YEAR = 1;
    private static final Integer UPDATED_YEAR = 2;

    private static final String DEFAULT_TOPICS = "AAAAAAAAAA";
    private static final String UPDATED_TOPICS = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRICE = 1;
    private static final Integer UPDATED_PRICE = 2;

    private static final byte[] DEFAULT_CONTENT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_CONTENT = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_CONTENT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_CONTENT_CONTENT_TYPE = "image/png";

    @Inject
    private DocumentEntityRepository documentEntityRepository;

    @Inject
    private DocumentEntityService documentEntityService;

    @Inject
    private DocumentEntitySearchRepository documentEntitySearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDocumentEntityMockMvc;

    private DocumentEntity documentEntity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DocumentEntityResource documentEntityResource = new DocumentEntityResource();
        ReflectionTestUtils.setField(documentEntityResource, "documentEntityService", documentEntityService);
        this.restDocumentEntityMockMvc = MockMvcBuilders.standaloneSetup(documentEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DocumentEntity createEntity(EntityManager em) {
        DocumentEntity documentEntity = new DocumentEntity()
                .title(DEFAULT_TITLE)
                .type(DEFAULT_TYPE)
                .review(DEFAULT_REVIEW)
                .author(DEFAULT_AUTHOR)
                .publisher(DEFAULT_PUBLISHER)
                .year(DEFAULT_YEAR)
                .topics(DEFAULT_TOPICS)
                .price(DEFAULT_PRICE)
                .content(DEFAULT_CONTENT)
                .contentContentType(DEFAULT_CONTENT_CONTENT_TYPE);
        return documentEntity;
    }

    @Before
    public void initTest() {
        documentEntitySearchRepository.deleteAll();
        documentEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createDocumentEntity() throws Exception {
        int databaseSizeBeforeCreate = documentEntityRepository.findAll().size();

        // Create the DocumentEntity

        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isCreated());

        // Validate the DocumentEntity in the database
        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeCreate + 1);
        DocumentEntity testDocumentEntity = documentEntityList.get(documentEntityList.size() - 1);
        assertThat(testDocumentEntity.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testDocumentEntity.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testDocumentEntity.getReview()).isEqualTo(DEFAULT_REVIEW);
        assertThat(testDocumentEntity.getAuthor()).isEqualTo(DEFAULT_AUTHOR);
        assertThat(testDocumentEntity.getPublisher()).isEqualTo(DEFAULT_PUBLISHER);
        assertThat(testDocumentEntity.getYear()).isEqualTo(DEFAULT_YEAR);
        assertThat(testDocumentEntity.getTopics()).isEqualTo(DEFAULT_TOPICS);
        assertThat(testDocumentEntity.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testDocumentEntity.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testDocumentEntity.getContentContentType()).isEqualTo(DEFAULT_CONTENT_CONTENT_TYPE);

        // Validate the DocumentEntity in ElasticSearch
        DocumentEntity documentEntityEs = documentEntitySearchRepository.findOne(testDocumentEntity.getId());
        assertThat(documentEntityEs).isEqualToComparingFieldByField(testDocumentEntity);
    }

    @Test
    @Transactional
    public void createDocumentEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = documentEntityRepository.findAll().size();

        // Create the DocumentEntity with an existing ID
        DocumentEntity existingDocumentEntity = new DocumentEntity();
        existingDocumentEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingDocumentEntity)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentEntityRepository.findAll().size();
        // set the field null
        documentEntity.setTitle(null);

        // Create the DocumentEntity, which fails.

        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isBadRequest());

        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentEntityRepository.findAll().size();
        // set the field null
        documentEntity.setType(null);

        // Create the DocumentEntity, which fails.

        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isBadRequest());

        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAuthorIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentEntityRepository.findAll().size();
        // set the field null
        documentEntity.setAuthor(null);

        // Create the DocumentEntity, which fails.

        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isBadRequest());

        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPublisherIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentEntityRepository.findAll().size();
        // set the field null
        documentEntity.setPublisher(null);

        // Create the DocumentEntity, which fails.

        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isBadRequest());

        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYearIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentEntityRepository.findAll().size();
        // set the field null
        documentEntity.setYear(null);

        // Create the DocumentEntity, which fails.

        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isBadRequest());

        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentEntityRepository.findAll().size();
        // set the field null
        documentEntity.setPrice(null);

        // Create the DocumentEntity, which fails.

        restDocumentEntityMockMvc.perform(post("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isBadRequest());

        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDocumentEntities() throws Exception {
        // Initialize the database
        documentEntityRepository.saveAndFlush(documentEntity);

        // Get all the documentEntityList
        restDocumentEntityMockMvc.perform(get("/api/document-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(documentEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].review").value(hasItem(DEFAULT_REVIEW.toString())))
            .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
            .andExpect(jsonPath("$.[*].publisher").value(hasItem(DEFAULT_PUBLISHER.toString())))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)))
            .andExpect(jsonPath("$.[*].topics").value(hasItem(DEFAULT_TOPICS.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].contentContentType").value(hasItem(DEFAULT_CONTENT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].content").value(hasItem(Base64Utils.encodeToString(DEFAULT_CONTENT))));
    }

    @Test
    @Transactional
    public void getDocumentEntity() throws Exception {
        // Initialize the database
        documentEntityRepository.saveAndFlush(documentEntity);

        // Get the documentEntity
        restDocumentEntityMockMvc.perform(get("/api/document-entities/{id}", documentEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(documentEntity.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.review").value(DEFAULT_REVIEW.toString()))
            .andExpect(jsonPath("$.author").value(DEFAULT_AUTHOR.toString()))
            .andExpect(jsonPath("$.publisher").value(DEFAULT_PUBLISHER.toString()))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR))
            .andExpect(jsonPath("$.topics").value(DEFAULT_TOPICS.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE))
            .andExpect(jsonPath("$.contentContentType").value(DEFAULT_CONTENT_CONTENT_TYPE))
            .andExpect(jsonPath("$.content").value(Base64Utils.encodeToString(DEFAULT_CONTENT)));
    }

    @Test
    @Transactional
    public void getNonExistingDocumentEntity() throws Exception {
        // Get the documentEntity
        restDocumentEntityMockMvc.perform(get("/api/document-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDocumentEntity() throws Exception {
        // Initialize the database
        documentEntityService.save(documentEntity);

        int databaseSizeBeforeUpdate = documentEntityRepository.findAll().size();

        // Update the documentEntity
        DocumentEntity updatedDocumentEntity = documentEntityRepository.findOne(documentEntity.getId());
        updatedDocumentEntity
                .title(UPDATED_TITLE)
                .type(UPDATED_TYPE)
                .review(UPDATED_REVIEW)
                .author(UPDATED_AUTHOR)
                .publisher(UPDATED_PUBLISHER)
                .year(UPDATED_YEAR)
                .topics(UPDATED_TOPICS)
                .price(UPDATED_PRICE)
                .content(UPDATED_CONTENT)
                .contentContentType(UPDATED_CONTENT_CONTENT_TYPE);

        restDocumentEntityMockMvc.perform(put("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDocumentEntity)))
            .andExpect(status().isOk());

        // Validate the DocumentEntity in the database
        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeUpdate);
        DocumentEntity testDocumentEntity = documentEntityList.get(documentEntityList.size() - 1);
        assertThat(testDocumentEntity.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testDocumentEntity.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testDocumentEntity.getReview()).isEqualTo(UPDATED_REVIEW);
        assertThat(testDocumentEntity.getAuthor()).isEqualTo(UPDATED_AUTHOR);
        assertThat(testDocumentEntity.getPublisher()).isEqualTo(UPDATED_PUBLISHER);
        assertThat(testDocumentEntity.getYear()).isEqualTo(UPDATED_YEAR);
        assertThat(testDocumentEntity.getTopics()).isEqualTo(UPDATED_TOPICS);
        assertThat(testDocumentEntity.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testDocumentEntity.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testDocumentEntity.getContentContentType()).isEqualTo(UPDATED_CONTENT_CONTENT_TYPE);

        // Validate the DocumentEntity in ElasticSearch
        DocumentEntity documentEntityEs = documentEntitySearchRepository.findOne(testDocumentEntity.getId());
        assertThat(documentEntityEs).isEqualToComparingFieldByField(testDocumentEntity);
    }

    @Test
    @Transactional
    public void updateNonExistingDocumentEntity() throws Exception {
        int databaseSizeBeforeUpdate = documentEntityRepository.findAll().size();

        // Create the DocumentEntity

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDocumentEntityMockMvc.perform(put("/api/document-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documentEntity)))
            .andExpect(status().isCreated());

        // Validate the DocumentEntity in the database
        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDocumentEntity() throws Exception {
        // Initialize the database
        documentEntityService.save(documentEntity);

        int databaseSizeBeforeDelete = documentEntityRepository.findAll().size();

        // Get the documentEntity
        restDocumentEntityMockMvc.perform(delete("/api/document-entities/{id}", documentEntity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean documentEntityExistsInEs = documentEntitySearchRepository.exists(documentEntity.getId());
        assertThat(documentEntityExistsInEs).isFalse();

        // Validate the database is empty
        List<DocumentEntity> documentEntityList = documentEntityRepository.findAll();
        assertThat(documentEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDocumentEntity() throws Exception {
        // Initialize the database
        documentEntityService.save(documentEntity);

        // Search the documentEntity
        restDocumentEntityMockMvc.perform(get("/api/_search/document-entities?query=id:" + documentEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(documentEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].review").value(hasItem(DEFAULT_REVIEW.toString())))
            .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
            .andExpect(jsonPath("$.[*].publisher").value(hasItem(DEFAULT_PUBLISHER.toString())))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)))
            .andExpect(jsonPath("$.[*].topics").value(hasItem(DEFAULT_TOPICS.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].contentContentType").value(hasItem(DEFAULT_CONTENT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].content").value(hasItem(Base64Utils.encodeToString(DEFAULT_CONTENT))));
    }
}
